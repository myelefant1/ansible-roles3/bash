## Bash

This role:
  * installs bash
  * installs bash-completion
  * configures `$TMUT` variable for automatic logout
  * configures timestamp history

## Role parameters

| name                    | value       | default_value   | description                                 |
| ------------------------|-------------|-----------------|---------------------------------------------|
| bash_update_packages    | boolean     | true            | update packages cache                       |
| bash_autologout_seconds | integer     | true            | num seconds of inactivity before autologout |


## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.
```yaml
---
- src: https://gitlab.com/ansible-roles3/bash.git
  scm: git
  version: v1.2
```

### Sample playbook

```yaml
- hosts: all
  roles:
      - role: bash
        bash_autologout_seconds: 100
        bash_update_packages: true
```
